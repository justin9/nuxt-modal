import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: {
            showModal: false
        },
        mutations: {
            toggleShowModal (state, showModal) {
                state.showModal = showModal
            }
        }
    })
}

export default createStore;
